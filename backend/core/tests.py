import random
import copy
from decimal import Decimal

from rest_framework import status
from rest_framework.test import APIClient
from django.test import TestCase
from django.contrib.auth import get_user_model

from core.models import OfficeSpace
from core.constants import LANDLORD
from core.serializers import OfficeSpaceSerializer

User = get_user_model()

client = APIClient()

OFF_COMMON_VALUES = {
    'name': 'Office Space',
    'street': 'Street 1',
    'city': 'City',
    'zip_code': '01-123',
    'lat': Decimal(random.randint(52, 53)),
    'lng': Decimal(random.randint(20, 21)),
    'public': random.choice([True, False])
}


class GetAllOrPublicOfficeSpaceTest(TestCase):
    """ Test module for GET office spaces API """

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        for i in range(5):
            data = copy.deepcopy(OFF_COMMON_VALUES)
            data['name'] = OFF_COMMON_VALUES['name'] + str(i)
            OfficeSpace.objects.create(**data)
        User.objects.create_user("test@test.com", "test", type=LANDLORD)

    @classmethod
    def tearDownClass(cls):
        client.logout()
        super().tearDownClass()

    @staticmethod
    def get_response_data():
        # get API response
        response = client.get('/office-spaces/')

        # get data from db
        offspc = OfficeSpace.objects.all()

        serializer = OfficeSpaceSerializer(offspc, many=True)
        return response, serializer

    def test_for_anonymous(self):
        # response should return only OfficeSpace.public=True
        response, serializer = self.get_response_data()
        self.assertNotEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_for_user(self):
        # response should return all the OfficeSpace
        client.login(username="test@test.com", password="test")
        response, serializer = self.get_response_data()
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

from decimal import Decimal
import random

from django.contrib.auth import get_user_model
from django.core.management import BaseCommand

from core.constants import LANDLORD, TENANT
from core.models import CustomUser, OfficeSpace

User = get_user_model()


def get_random_int():
    return random.randint(0, 99999)


def office_space_factory(number):
    building_names = [
        'Central Tower', 'Centrum LIM', 'Generation Park', 'Intraco I',
        'Millennium Plaza', 'Oxford Tower', 'Palace of Culture and Science',
    ]
    for i in range(number):
        yield {
            'name': "{}_{}".format(building_names[i % len(building_names)], get_random_int()),
            'street': 'Street 1',
            'city': 'City',
            'zip_code': '01-123',
            'lat': Decimal(random.randint(52, 53)),
            'lng': Decimal(random.randint(20, 21)),
        }


def user_factory(number):
    """
    :param number: no of users to be created
    :return: tuple of args and kwargs
    """
    user_type = random.choice([LANDLORD, TENANT])

    def get_email(num):
        if user_type == LANDLORD:
            return "lan{}@lan.lan".format(num)
        return "ten{}@ten.ten".format(num)

    for i in range(number):
        yield (get_email(i),), {'type': user_type}


class Command(BaseCommand):
    """
    Populates local database for development with sample random
    objects.

    Never to be used in production.

    """
    def add_arguments(self, parser):
        parser.add_argument(
            '--office-spaces',
            type=int,
            default=10
        )
        parser.add_argument(
            '--users',
            type=int,
            default=5
        )

    def handle(self, *args, **options):

        for office_space in office_space_factory(options['office_spaces']):
            OfficeSpace.objects.create(**office_space)

        for uargs, ukwrgs in user_factory(options['users']):
            User.objects.create_user(*uargs, **ukwrgs)

        # create superuser
        User.objects.create_superuser("admin@admin.com", "admin", type=LANDLORD)

        print("Populated successfully !")

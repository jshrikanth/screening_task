from rest_framework import viewsets, permissions

from core.models import OfficeSpace
from core.serializers import OfficeSpaceSerializer


class OfficeSpaceViewSet(viewsets.ModelViewSet):
    serializer_class = OfficeSpaceSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def get_queryset(self):
        """
        this will return all the office space if not user, then only public office spaces
        """
        queryset = OfficeSpace.objects.all()

        if not self.request.user.is_authenticated:
            queryset = queryset.filter(public=True)
        return queryset

from rest_framework import serializers

from core.models import OfficeSpace


class OfficeSpaceSerializer(serializers.ModelSerializer):
    class Meta:
        model = OfficeSpace
        fields = ('id', 'name', 'city', 'zip_code', 'lat', 'lng')

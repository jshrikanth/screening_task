#!/bin/bash
set -ex

python3 manage.py reset_db --noinput
python3 manage.py migrate
python3 manage.py populate $@

from django.contrib import admin
from django.urls import include, path
from django.conf import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/', include('rest_framework.urls')),
    path('', include('core.urls'))
]

if settings.DEBUG:
    urlpatterns += [path('debug', include('debug_toolbar.urls'))]


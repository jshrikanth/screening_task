from .base import *

SECRET_KEY = "0n13h@3_n@!a4m%^&^--saj#mm$=8ncasd9p2yxer$t4y$*!re"

DEBUG = False

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'ob',
        'USER': 'test',
        'PASSWORD': 'tEsT_123',
        'HOST': 'db',
        'PORT': '5432',
    }
}


STATIC_ROOT = '/var/static'
MEDIA_ROOT = '/var/media/pictures'

from .base import *

DEBUG = True

LOCAL_APPS = [
    'django_extensions',
    'debug_toolbar'
]

LOCAL_MIDDLEWARE = [
    'debug_toolbar.middleware.DebugToolbarMiddleware',
]

INSTALLED_APPS += LOCAL_APPS

MIDDLEWARE += LOCAL_MIDDLEWARE
# officebird.com


Office Bird is an app facilitating subletting of office space between landlords
and potential tenants.

Landlord creates buildings, and tenants browse and participate in the ones they find
interesting

# local API development

    ln -s ../../pre-commit.hook.py .git/hooks/pre-commit
    apt install postgres
    sudo su postgres -c "createuser -P $USER"
    createdb ob
    mkvirtualenv -r backend/requirements_dev.txt -p/usr/bin/python3.6 venv
    cd backend
    ./manage.py makemigrations
    ./flush_db_and_populate.sh # to reset db and populate sample data
    ./runtests.py # to run all the tests
    
    To start with simple api, 
        curl http://localhost:8000/office-spaces/
    
    
    
